/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import java.lang.reflect.Array;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.Map;

import ar.com.ank.lang.r4.enums.OpCodeType;

public class OpCode {
	private OpCodeType type;
	private String text;
	private Map params = null;
	public OpCode(OpCodeType newType, String newText) {
		this(newType, newText, (Map) null);
	}
	public OpCode(OpCodeType newType, String newText, Map newParams) {
		if (newType == null) {
		    throw(new IllegalArgumentException("need a type!"));
		}
		if (newText == null) {
		    throw(new IllegalArgumentException("need a text!"));
		}
		this.type = newType;
		this.text = newText;
		this.params = newParams;
	}
	public OpCodeType getType() {
		return type;
	}
	public String getText() {
		return text;
	}
    public String toString() {
    	String s = "("+type+",\""+Utils.quoteString(text)+"\"";
    	if (params != null && !params.isEmpty()) {
    		s+=",";
    		for (Iterator iter = params.keySet().iterator(); iter.hasNext();) {
				String key = (String) iter.next();
				s+=key+"="+params.get(key).toString();
			}
    	}
    	return s+")";
    }
    public Map getParams() {
    	return params;
    }
    public int[] toIntArray() {
    	int[] r = Utils.stringToIntArray(text);
    	int[] q = new int[r.length+1];
    	q[0] = type.toInt();
    	for (int i = 0; i < r.length; i++) {
    		q[i+1] = r[i];
    	}
    	return q;
    }
}
