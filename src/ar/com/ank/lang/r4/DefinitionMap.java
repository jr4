/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import java.util.Map;
import java.util.Hashtable;
import java.util.Set;
import java.util.Iterator;

import ar.com.ank.lang.r4.datatypes.Pointer;
import ar.com.ank.lang.r4.Utils;

public class DefinitionMap {
    private Map map = new Hashtable();
    /** a null value will remove the entry */
    public void define(String name, Pointer value) {
	if (value == null) {
	    map.remove(name);
	} else {
	    map.put(name,new Definition(name,value));
	}
    }
    public void undefine(String name) {
	if (isDefined(name)) {
	    this.define(name, null);
	} else {
	    throw new IllegalArgumentException("Tried to undefine a value (\""+name+"\") that's already undefined.");
	}
    }
    public boolean isDefined(String name) {
	return map.containsKey(name);
    }
    public Definition get(String name) {
	return (Definition) map.get(name);
    }
    public String toString() {
	if (null == map) {
	    return "DEFINITIONMAP = (null)";
	}
	if (map.isEmpty()) {
	    return "DEFINITIONMAP = (empty)";
	}
	String r = "DEFINITIONMAP =\n";
	Set s = map.entrySet();
	for (Iterator iter = s.iterator(); iter.hasNext();) {
	    Map.Entry e = (Map.Entry) iter.next();
	    String name = (String) e.getKey();
	    Definition def = (Definition) e.getValue();
	    r += " - \"" + Utils.quoteString(name) + "\" is [" + def+ "]\n";
	}

	return r;
    }
}
