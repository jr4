/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import ar.com.ank.lang.r4.datatypes.IntValue;
import ar.com.ank.lang.r4.datatypes.Pointer;
import ar.com.ank.lang.r4.datatypes.Value;
import ar.com.ank.lang.r4.enums.OpCodeType;
import ar.com.ank.lang.r4.enums.TokenType;

public class Interpreter {
    private Environment environment;

    public Interpreter() {
	environment = new Environment();
    }

    /** Compile and execute the program */
    public void run(String program) {
	List prog = compile(program);
	execute(prog);
    }

    // Executes a list of OpCodes
    public void execute(List tokens) {
	for (Iterator i = tokens.iterator(); i.hasNext() ;) {
	    OpCode o = (OpCode) i.next();
	    System.out.println(o);
	    execute(o);
	    System.out.println(environment.getStack().dump());
	    }
	}
    
    // Executes one OpCode
    public void execute(OpCode o) {
	    if (o.getType().equals(OpCodeType.NUMBER)) {
			    op_push(new IntValue(Integer.parseInt(o.getText())));
	    } else if (o.getType().equals(OpCodeType.START)) {
	    	// TODO - IGNORED!
	    } else if (o.getType().equals(OpCodeType.SUM)) {
	    	op_sum();
	    } else if (o.getType().equals(OpCodeType.SUB)) {
		    op_sub();
	    } else if (o.getType().equals(OpCodeType.MUL)) {
	    	op_mul();
	    } else if (o.getType().equals(OpCodeType.DIV)) {
	    	op_div();

	    	
	    } else if (o.getType().equals(OpCodeType.OVER)) {
	    	op_over();
	    } else if (o.getType().equals(OpCodeType.DUP)) {
	    	op_dup();
	    } else if (o.getType().equals(OpCodeType.DROP)) {
	    	op_drop();
	    } else if (o.getType().equals(OpCodeType.ROT)) {
	    	op_rot();
	    } else if (o.getType().equals(OpCodeType.PICK2)) {
	    	op_pick2();
	    } else if (o.getType().equals(OpCodeType.PICK3)) {
	    	op_pick3();
	    } else if (o.getType().equals(OpCodeType.PICK4)) {
	    	op_pick4();
	    } else if (o.getType().equals(OpCodeType.SWAP)) {
	    	op_swap();
	    } else if (o.getType().equals(OpCodeType.NIP)) {
	    	op_nip();

	    } else if (o.getType().equals(OpCodeType._2DUP)) {
	    	op_2dup();
	    } else if (o.getType().equals(OpCodeType._2DROP)) {
	    	op_2drop();
	    } else if (o.getType().equals(OpCodeType._3DROP)) {
	    	op_3drop();
	    } else if (o.getType().equals(OpCodeType._4DROP)) {
	    	op_4drop();
	    } else if (o.getType().equals(OpCodeType._2OVER)) {
	    	op_2over();
	    } else if (o.getType().equals(OpCodeType._2SWAP)) {
	    	op_2swap();
	    } else if (o.getType().equals(OpCodeType._0Q)) {
	    	op_0q(o); // TODO FINISHME
	    } else if (o.getType().equals(OpCodeType._1Q)) {
	    	op_1q(o); // TODO FINISHME

	    	
	    } else {
	    	runtimeError("Unhandled OpCode type: "+o);
	    }
    }

    // Given a string of a program or part of a program, compile into OpCodes
    public List compile(String text) {
	List result = new ArrayList();
	
    // Tokenize and filter useless tokens
	Tokenizer tok = new Tokenizer(text);
	List tokens = new ArrayList();
	for (Token t = tok.getToken(); t.getType().notEquals(TokenType.EOF); t = tok.getToken()) {
		if (!t.getType().equals(TokenType.SPACE) && !t.getType().equals(TokenType.COMMENT) && !t.getType().equals(TokenType.EOL) ) {
			tokens.add(t);
			System.out.println(t);
		}
	}
	//System.exit(0);

	// Compile to opcodes
	for (int tokenNum = 0; tokenNum<tokens.size(); tokenNum++) {
		// If we found an OpCode, copy it verbatim.
		if (tokens.get(tokenNum) instanceof OpCode) {
			result.add( (OpCode) tokens.get(tokenNum) );
			continue;
		}
		Token t = (Token) tokens.get(tokenNum);
	    if (t.getType().equals(TokenType.DEF_START)) {
		Pointer freeMem = environment.getFreeMemStart();
		environment.getDefinitions().define(t.getText(),freeMem);
		continue;
	    } 
	    if (t.getType().equals(TokenType.TERM)) {
	    	if (t.isInternalOperation()) {
	    		if (t.getText().equals("+")) {
			    	result.add(new OpCode(OpCodeType.SUM, t.getText()));
	    		} else if (t.getText().equals("-")) {
	    			result.add(new OpCode(OpCodeType.SUB, t.getText()));
	    		} else if (t.getText().equals("*")) {
	    			result.add(new OpCode(OpCodeType.MUL, t.getText()));
	    		} else if (t.getText().equals("/")) {
	    			result.add(new OpCode(OpCodeType.DIV, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("over")) {
	    			result.add(new OpCode(OpCodeType.OVER, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("dup")) {
	    			result.add(new OpCode(OpCodeType.DUP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("drop")) {
	    			result.add(new OpCode(OpCodeType.DROP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("ROT")) {
	    			result.add(new OpCode(OpCodeType.ROT, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("PICK2")) {
	    			result.add(new OpCode(OpCodeType.PICK2, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("PICK3")) {
	    			result.add(new OpCode(OpCodeType.PICK3, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("PICK4")) {
	    			result.add(new OpCode(OpCodeType.PICK4, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("SWAP")) {
	    			result.add(new OpCode(OpCodeType.SWAP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("NIP")) {
	    			result.add(new OpCode(OpCodeType.NIP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("2dup")) {
	    			result.add(new OpCode(OpCodeType._2DUP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("2drop")) {
	    			result.add(new OpCode(OpCodeType._2DROP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("3drop")) {
	    			result.add(new OpCode(OpCodeType._3DROP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("4drop")) {
	    			result.add(new OpCode(OpCodeType._4DROP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("2over")) {
	    			result.add(new OpCode(OpCodeType._2OVER, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("2swap")) {
	    			result.add(new OpCode(OpCodeType._2SWAP, t.getText()));
	    		} else if (t.getText().equalsIgnoreCase("0?")) {
	    			Token nextToken = (Token) tokens.get(tokenNum+1);
	    			if (nextToken == null || !nextToken.getType().equals(TokenType.OPEN_PARENS)) {
	    				// not followed by (
	    				compilationError("opening parens not found after conditional");
	    			}
	    			int elsePos = findMatchingElse(tokens,tokenNum+1);
	    			int parensPos = findMatchingParens(tokens,tokenNum+1);
	    			if (parensPos==-1) {
	    				compilationError("Unmatched parens!");
	    			} else {
	    				Map params = new HashMap();
	    				params.put("else", new Integer(elsePos-tokenNum));
	    				result.add(new OpCode(OpCodeType._0Q, t.getText(), params));
	    				
	    				params = new HashMap();
	    				params.put("to", new Integer(parensPos-elsePos));
	    				tokens.set(elsePos, new OpCode(OpCodeType.JMP, "", params));
	    				tokens.remove(parensPos);
	    				tokens.remove(tokenNum+1);
	    			}
	    		} else if (t.getText().equalsIgnoreCase("1?")) {
	    			// TODO FINISHME	    			
	    			result.add(new OpCode(OpCodeType._1Q, t.getText()));

	    		} else if (t.getText().equalsIgnoreCase(")(")) {
	    			// TODO FINISHME
	    			result.add(new OpCode(OpCodeType._1Q, t.getText()));

	    		} else {
	    			compilationError("Unmanaged token: "+t);			
	    		}
	    	} else if (t.isNumeric()) {
		    	result.add(new OpCode(OpCodeType.NUMBER, t.getText()));
	    	}
	    	continue;
	    } 
	    if (t.getType().equals(TokenType.START)) {
	    	result.add(new OpCode(OpCodeType.START, t.getText()));
	    	continue;
	    }
	    
	    // TODO FIXME PARENS
	    if (t.getType().equals(TokenType.OPEN_PARENS) || t.getType().equals(TokenType.CLOSE_PARENS)) {
	    	continue;
	    }
	    if (t.getType().equals(TokenType.END)) {
	    	// TODO FIXME - END is not always the end
	    	result.add(new OpCode(OpCodeType.EXIT, t.getText()));
	    	continue;
	    }	    
	    compilationError("Unmanaged token: "+t);
	}

	return result;
    }

    /** Given a token list and the position of a parens, return the position where the matching end parens is or -1*/
    private int findMatchingParens(List tokens, int tokenNum) {
    	int parensLevel = 0;
    	for (int i = tokenNum; i<tokens.size(); i++) {
    		Token t = (Token) tokens.get(i);
    		if (t.getType().equals(TokenType.OPEN_PARENS)) {
    			parensLevel++;
    		} else if (t.getType().equals(TokenType.CLOSE_PARENS)) {
    			parensLevel--;
    			if (parensLevel==0) {
    				return i;
    			}
    		}
    	}
    	return -1;
    }

    /** Given a token list and the position of a parens, return the position where the matching ")(" is or -1*/
    private int findMatchingElse(List tokens, int tokenNum) {
    	// TODO finishme
    	int parensLevel = 0;
    	for (int i = tokenNum; i<tokens.size(); i++) {
    		Token t = (Token) tokens.get(i);
    		if (t.getType().equals(TokenType.OPEN_PARENS)) {
    			parensLevel++;
    		} else if (t.getType().equals(TokenType.CLOSE_PARENS)) {
    			parensLevel--;
    		}
			if (parensLevel==1 && t.getType().equals(TokenType.ELSE)) {
				return i;
			}
    	}
    	return -1;
    }

    
    private void runtimeError(String s) {
    	System.err.println("Runtime error: "+s);
    	System.exit(0);
    }
    private void compilationError(String s) {
    	System.err.println("Compilation error: "+s);
    	System.exit(0);
    }

    public Environment getEnvironment() {
    	return environment;
    }

    
    // *************** OPERATIONS START
    // TODO: what about divzero, overflow, underflow, etc?
    // TODO: what about stack over/underflow?
    // TODO: what about having more than one : (start)?
    private void op_push(Value t) { push(t); }
    private void op_sum() { push(ipop()+ipop()); }
    private void op_sub() { push(-ipop()+ipop()); }
    private void op_mul() { push(ipop()*ipop()); }
    private void op_div() {
    	// TODO - this is integer division only
    	int a = ipop();
    	push( ipop() / a );
    }
    private void op_over() { push(get(1)); }
    private void op_dup() { push(get(0)); }
    private void op_drop() { pop(); }
    private void op_rot() {
    	Value c = pop();
    	Value b = pop();
    	Value a = pop();
    	push(b);
    	push(c);
    	push(a);
    }
    private void op_pick2() { push(iget(2)); }
    private void op_pick3() { push(iget(3)); }
    private void op_pick4() { push(iget(4)); }
    private void op_swap() { Value b = pop(); Value a = pop(); push(b); push(a); }
    private void op_nip() { Value a = pop(); pop(); push(a);  }
    private void op_2dup() { op_over(); op_over(); }
    private void op_2drop() { op_drop(); op_drop(); }
    private void op_3drop() { op_drop(); op_drop(); op_drop(); }
    private void op_4drop() { op_drop(); op_drop(); op_drop(); op_drop(); }
    private void op_2over() { push(get(3)); push(get(3)); }
    private void op_2swap() {
    	Value d = pop();
    	Value c = pop();
    	Value b = pop();
    	Value a = pop();
    	push(c);
    	push(d);
    	push(a);
    	push(b);
    }
    private void op_0q(OpCode o) { // 0?
    	if (iget(0)!=0) {
    		environment.advanceInstructionPointer(((Integer)o.getParams().get("else")).intValue());
    	}
    }
    private void op_1q(OpCode o) { // 1?
    	if (iget(0)!=0) {
    		// TODO no jump
    	} else {
    		// TODO jump    		
    	}
    }
    
    // Convenience stuff
//    private Stack s() { return environment.getStack(); }
    private void push(Value v) { environment.getStack().push(v); }
    private void push(int i) { environment.getStack().push(new IntValue(i)); }
    private Value pop() { return environment.getStack().pop(); }
    private int ipop() { return ((IntValue)environment.getStack().pop()).getAsInt(); }
    private Value get(int i) { return environment.getStack().get(i); }
    private int iget(int i) { return ((IntValue)environment.getStack().get(i)).getAsInt(); }
    // End of convenience stuff
    
    // *************** OPERATIONS END

}