/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import java.io.*;

import ar.com.ank.lang.r4.datatypes.IntValue;
import ar.com.ank.lang.r4.datatypes.Value;

public class Main {
    public static void main(String[] cmd) {
//	final String FILE_NAME = "c:/Documents and Settings/andres.kievsky/tmp/test.r4";
// TODO: "4 ++" "4 +-" --> wtf!
    	// TODO "0? ( 1? ( 2 )( 3 ) 4 )( 5 )"
//	String program = read_file(FILE_NAME);
	//String program = ": 1 2 3 4 2 2 + 3 - 3 + 4 / 10 * + + + + 30 +";
	// String program = ": 1 2 3 4 drop over dup";
//    String program = ": 1 2 3 4 rot";
//    String program = ": 1 2 3 pick2";
//    String program = ": 1 2 3 4 pick3";
//  String program = ": 1 2 3 4 5 pick4";    
//    String program = ": 1 2 swap nip";
    // "2dup", "2drop", "3drop", "4drop", "2over", "2swap",
//    String program = ": 1 2 2dup";
    // String program = ": 1 2 3 2drop";
//    String program = ": 1 2 3 3drop";
//    String program = ": 1 2 3 4 4drop";
//    	String program = ": 1 2 3 4 2over";
//    	String program = ": 1 2 3 4 2swap";
    	
    String program = "4 2 / 0? (5 6 7 8)(6)";
    
    	
//    	String program = "0 0? ( 1? ( 2 )( 3 ) 4 )( 5 )";
    	
    	
	//:suma 2 2 + ;  :sum 2 + ; deberia dejar 1
	// "contar 10 ( 1? )( 1- dup ) ; : contar ;";
	// :pruebaif  1? ( "no es cero" )( "es cero" ) nip print ;
	Interpreter intr = new Interpreter();
	intr.run(program);
    }

    private static String read_file(String fName) {
	String program = "";
	try {
	    BufferedReader input = new BufferedReader(new FileReader(fName));
	    int thisLine;
	    while ((thisLine = input.read()) != -1) { program = program + (char)thisLine; }
	    input.close();
	} catch (IOException e) {
	    System.err.println("Error: "+e);
	    System.exit(1);
	}
	return program;
    }

}