package ar.com.ank.lang.r4.datatypes;

import java.io.Serializable;

public class IntValue extends Value {

	private int content = 0;
	
	public String toString() {
		return Integer.toString(content);
	}

	public IntValue(int value) {
		content = value;
	}
	public IntValue(byte[] b) {
		content = deserialize(b);
	}

	public int getAsInt() {
		return content;
	}
	public byte[] serialize() {
		return new byte[] {
				(byte)(content & 0xFF),
				(byte)((content & 0xFF00)>>>8),
				(byte)((content & 0xFF0000)>>>16),
				(byte)((content & 0xFF000000)>>>24)};
	}
	public static int deserialize(byte[] buf) {
    	return ((buf[3] & 0xFF) << 24)
        | ((buf[2] & 0xFF) << 16)
        | ((buf[1] & 0xFF) << 8)
        | (buf[0] & 0xFF);
	}
	
}
