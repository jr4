/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4.datatypes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * Represents a value. Should be consistent with the flyweight pattern.
 * @author andres.kievsky
 *
 */

public abstract class Value {

	private byte[] content = null;
	
	/** Create a new instance based on a byte array */
	public static Value newInstace(byte[] b) {
		return null;
	}
	
	public byte[] getContent() {
		return content;
	}
	protected void setContent(byte[] c) {
		content = c;
	}
	
	public abstract byte[] serialize();
	 
	public abstract String toString();
}
