/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4.enums;

public class OpCodeType extends Enumeration {
    public static final OpCodeType NUMBER = new OpCodeType("NUMBER");
    
    public static final OpCodeType STRING = new OpCodeType("STRING");
    public static final OpCodeType EXIT = new OpCodeType("EXIT");
    public static final OpCodeType START = new OpCodeType("START");
    
    // Internal operations
    public static final OpCodeType SUM = new OpCodeType("SUM"); // +
    public static final OpCodeType SUB = new OpCodeType("SUB"); // -
    public static final OpCodeType MUL = new OpCodeType("MUL"); // *
    public static final OpCodeType DIV = new OpCodeType("DIV"); // /
    
    public static final OpCodeType OVER = new OpCodeType("OVER");
    public static final OpCodeType DUP = new OpCodeType("DUP");
    public static final OpCodeType DROP = new OpCodeType("DROP");
    public static final OpCodeType ROT = new OpCodeType("ROT");
    public static final OpCodeType PICK2 = new OpCodeType("PICK2");
    public static final OpCodeType PICK3 = new OpCodeType("PICK3");
    public static final OpCodeType PICK4 = new OpCodeType("PICK4");
    public static final OpCodeType SWAP = new OpCodeType("SWAP");
    public static final OpCodeType NIP = new OpCodeType("NIP");
    public static final OpCodeType _2DUP = new OpCodeType("_2DUP");
    public static final OpCodeType _2DROP = new OpCodeType("_2DROP");
    public static final OpCodeType _3DROP = new OpCodeType("_3DROP");
    public static final OpCodeType _4DROP = new OpCodeType("_4DROP");
    public static final OpCodeType _2OVER = new OpCodeType("_2OVER");
    public static final OpCodeType _2SWAP = new OpCodeType("_2SWAP");
    public static final OpCodeType _0Q = new OpCodeType("_0Q"); // 0?
    public static final OpCodeType _1Q = new OpCodeType("_1Q"); // 1?
    
    
    // VM-like constructs.
    public static final OpCodeType JMP = new OpCodeType("JMP"); // JMP x opcodes
    
    private OpCodeType(String name) {
    	super(name);
    }
    private OpCodeType(String name, int id) {
    	super(name, id);
    }

    public int toInt() {
        return id;
    }
    
    public String toString() {
    	return name + "#" + id;
    }

}
