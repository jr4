/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4.enums;

public class TokenType extends Enumeration {
    public static final TokenType COMMENT = new TokenType("COMMENT");
    public static final TokenType STRING = new TokenType("STRING");
    public static final TokenType EOL = new TokenType("EOL");
    public static final TokenType SPACE = new TokenType("SPACE");
    public static final TokenType START = new TokenType("START");
    public static final TokenType END = new TokenType("END");
    public static final TokenType OPEN_PARENS = new TokenType("OPEN_PARENS");
    public static final TokenType CLOSE_PARENS = new TokenType("CLOSE_PARENS");
    public static final TokenType ELSE = new TokenType("ELSE"); // )(
    public static final TokenType TERM = new TokenType("TERM");
    public static final TokenType DEF_START = new TokenType("DEF_START");
    public static final TokenType EOF = new TokenType("EOF");
    public static final TokenType VARDEF_START = new TokenType("VARDEF_START");

    private TokenType(String name) {
    	super(name);
    }
    private TokenType(String name, int id) {
    	super(name, id);
    }
    
}