/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ar.com.ank.lang.r4.enums;

/**
 * @author andres.kievsky
 *
 */
public abstract class Enumeration {
    protected final String name;
    protected final int id;
    protected static int lastId = 0;

    protected Enumeration(String name, int id) {
		this.name = name;
		this.id = id;
    }
    protected Enumeration(String name) {
		this.name = name;
		this.id = lastId++;
    }
    public String toString() {
    	return this.name;
    }
    public String getName() {
    	return this.name;
    }
    public int getId() {
    	return this.id;
    }
    public boolean equals(TokenType b) {
		if (b == null) { return false; }
		if (this.id == b.id) { return true; }
		return false;
    }
    public boolean notEquals(TokenType b) {
    	return !equals(b);
    }
    
}
