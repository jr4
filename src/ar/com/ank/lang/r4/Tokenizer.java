/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import ar.com.ank.lang.r4.enums.TokenType;

public class Tokenizer {
    private String text;
    private int position = 0;
    private int lineNumber = 1;

    private boolean stateStringMode = false;
    private int stateStringMode_firstDblQuote = 0;
    
    private boolean stateCommentMode = false;
    private int stateCommentMode_first = 0;

    private boolean stateTermMode = false;
    private int stateTermMode_first = 0;

    private boolean stateDefinitionNameMode = false;
    private int stateDefinitionNameMode_first = 0;

    private boolean stateVarDefinitionNameMode = false;
    private int stateVarDefinitionNameMode_first = 0;

    public Tokenizer(String s) {
	text = s;
    }
    public Token getToken() {
	if (!pastEnd()) {
	    String currentChar = getCurrentChar();
	    
	    // Keep track of line number
	    if (isEOLChar(currentChar)) {
		// TODO fixme windows lines, etc
		lineNumber++;
	    }

	    // Comments
	    if (!stateCommentMode) {
		if (currentChar.equals("|")) {
		    stateCommentMode = true;
		    stateCommentMode_first = position;
		    advancePosition();
		    return getToken();
		}
	    } else {
		String following = getFollowingChar();
		if (following == null || isEOLChar(following)) {
		    stateCommentMode = false;
		    advancePosition();
		    return new Token(TokenType.COMMENT, text.substring(stateCommentMode_first, position));
		} else {
		    advancePosition();
		    return getToken();
		}
	    }

	    // Double quotes
	    if (stateStringMode) {
		if (currentChar.equals("\"")) {
		    stateStringMode = false;
		    advancePosition();
		    return new Token(TokenType.STRING, text.substring(stateStringMode_firstDblQuote+1, position-1));
		} else {
		    advancePosition();
		    return getToken();
		}
	    } else {
		if (currentChar.equals("\"")) {
		    stateStringMode = true;
		    stateStringMode_firstDblQuote = position;
		    advancePosition();
		    return getToken();
		}
	    }

	    if (currentChar.equals("\r")) {
		String following = getFollowingChar();
		    advancePosition();
		if ("\n".equals(following)) {
		    advancePosition();
		    return new Token(TokenType.EOL,"\r\n");
		} else {
		    return new Token(TokenType.EOL,"\r");
		}
	    } else if (currentChar.equals("\n")) {
		advancePosition();
		return new Token(TokenType.EOL,"\n");
	    } else if (isSpaceChar(currentChar)) {
		String following = getFollowingChar();
		advancePosition();
		if (isSpaceChar(following)) {
		    Token nextToken = getToken();
		    return new Token(TokenType.SPACE,currentChar + nextToken.getText());
		} else {
		    return new Token(TokenType.SPACE,currentChar);
		}
	    } else if (currentChar.equals(":")) {
		// definition start or program start
		String following = getFollowingChar();
		advancePosition();
		if (isSpaceChar(following) || isEOLChar(following)) {
		    return new Token(TokenType.START, ":");
		} else {
		    stateDefinitionNameMode = true;
		    stateDefinitionNameMode_first = position-1;
		    return getToken();
		}
	    } else if (currentChar.equals("#")) {
		// variable definition start
		String following = getFollowingChar();
		advancePosition();
		if (isSpaceChar(following) || isEOLChar(following)) {
		    if (stateVarDefinitionNameMode) {
		    } else {
		    	tokenizerError("Unrecognized token \"#\"");
		    }
		} else {
		    stateVarDefinitionNameMode = true;
		    stateVarDefinitionNameMode_first = position-1;
		    return getToken();
		}
	    } else if (currentChar.equals("(")) {
		// OPEN_PARENS
		advancePosition();
		return new Token(TokenType.OPEN_PARENS, "(");
	    } else if (currentChar.equals(")")) {
    		// TODO: following should always be checked for nullity!! check check check!
	    	String following = getFollowingChar();
	    	if (following != null && following.equals("(")) {
	    		advancePosition();
	    		advancePosition();
	    		return new Token(TokenType.ELSE, ")(");
	    	} else {
	    		// CLOSE_PARENS
	    		advancePosition();
	    		return new Token(TokenType.CLOSE_PARENS, ")");
	    	}
	    } else if (currentChar.equals(";")) {
		// END: definition end or program end
		advancePosition();
		return new Token(TokenType.END, ";");
	    } else if (isTermChar(currentChar)) {

		if (stateDefinitionNameMode) {
		    String following = getFollowingChar();
		    advancePosition();
		    if (isSpaceChar(following) || isEOLChar(following)) {
			stateDefinitionNameMode = false;
			return new Token(TokenType.DEF_START, text.substring(stateDefinitionNameMode_first+1, position));
		    } else {
			return getToken();
		    }
		}

		if (stateTermMode) {
		    String following = getFollowingChar();
		    advancePosition();
		    if (isTermChar(following)) {
			return getToken();
		    } else {
			stateTermMode = false;
			return new Token(TokenType.TERM, text.substring(stateTermMode_first, position));
		    }
		} else {
		    stateTermMode = true;
		    stateTermMode_first = position;
		    String following = getFollowingChar();
		    advancePosition();
		    if (isTermChar(following)) {
			return getToken();
		    } else {
			stateTermMode = false;
			return new Token(TokenType.TERM, currentChar);
		    }
		}
	    } else {
	    	tokenizerError("Unrecognized token \""+currentChar+"\"");
	    }
	} else {
	    // reached end. Make sure the state is correct
	    if (stateStringMode) {
		// TODO FIXME
	    	tokenizerError("Unmatched parenthesis");
		return null;
	    }
	    if (stateCommentMode) {
		// NOTHING
	    }
	    return new Token(TokenType.EOF, "");
	}
	return null;
    }

    private boolean isTermChar(String c) {
	if (c == null) { return false; }
	if (c.matches("[a-zA-Z0-9]")) { return true; }
	if (c.equals("*") || c.equals("+") || c.equals("?") || c.equals("-") || c.equals("/") || c.equals("<") || c.equals(">") || c.equals("=") || c.equals("_")) {
	    return true;
	}
	return false;
    }
    private boolean isNumericChar(String c) {
	if (c == null) { return false; }
	if (c.matches("[0-9]")) { return true; }
	return false;
    }
    private boolean isEOLChar(String c) {
	if (c == null) { return false; }
	if (c.equals("\n") || c.equals("\r")) {
	    return true;
	}
	return false;
    }
    private boolean isSpaceChar(String c) {
    	if (c == null) { return false; }
    	if (c.equals(" ") || c.equals("\t")) {
    		return true;
    	}
    	return false;
    }

    private void tokenizerError(String msg) {
	// TODO FIXME
	System.err.println("Tokenizer Error - line "+lineNumber+": "+msg);
	System.exit(0);
    }
    public boolean pastEnd() {
	if (position>=text.length()) {
	    return true;
	} else {
	    return false;
	}
    }
    public boolean pastEnd(int pos) {
	if (pos>=text.length()) {
	    return true;
	} else {
	    return false;
	}
    }
    public String getCurrentChar() {
	if (pastEnd()) {
	    return null;
	} else {
	    return text.substring(position,position+1);
	}
    }
    /** get the char after the current one */
    public String getFollowingChar() {
	return getRelativeChar(1);
    }
    /** get a char, based on a position relative to the current one */
    public String getRelativeChar(int relativePos) {
	int absolutePos = position+relativePos;
	if (absolutePos<0 || pastEnd(absolutePos)) {
	    return null;
	} else {
	    return text.substring(absolutePos,absolutePos+1);
	}
    }
    public void advancePosition() {
	position++;
    }
    public int getLineNumber() {
	return lineNumber;
    }
}

