/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import ar.com.ank.lang.r4.datatypes.Pointer;

public class Definition {
    private String name; // may be null
    private Pointer pos;

    public Definition(String newName, Pointer pos) {
	this.name = newName;
	this.pos = pos;
    }

    /** Create an anonymous definition */
    public Definition(Pointer pos) {
	this.name = null;
	this.pos = pos;
    }

    public Pointer getPos() {
	return this.pos;
    }
    public String getName() {
	return this.name;
    }
    public String toString() {
	return "\"" + name + "\" -> " + pos;
    }

}
