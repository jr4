/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import java.util.Map;
import java.util.Hashtable;

import ar.com.ank.lang.r4.enums.TokenType;

public class Token {
    private TokenType type;
    private String text;
    private static String[] internalOperationsList = new String[] {
	// Stack operations - all implemented
   	"over", "dup", "drop", "rot", "pick2", "pick3", "pick4", "swap", "nip", "2dup", "2drop", "3drop", "4drop", "2over", "2swap", 

	// ? Conditional stuff - under development, none implemented yet.
	"0?", "+?", "=?", ">?", "<=?", "1?", "-?", "<>?", "<?", ">=?",

	// Logic stuff - none implemented yet.
	"and", "or", "xor", "not",

	// Arithmetic
	"+", "-", "/", "*",	// implemented
	
	"*/", "/mod", "mod", "neg", "1+", "1-", "2/", "2*", "<<", ">>", "*>>", // not implemented yet.

	// "new" - none implemented yet
	"and?", "nand?",
	
	// proprietary - none implemented yet
	".",

    };
    private static Map internalOperationsMap;
    static {
	internalOperationsMap = new Hashtable();
	for (int i = 0; i < internalOperationsList.length; i++) {
	    internalOperationsMap.put(internalOperationsList[i],new Integer(i));
	}
    }

    public Token(TokenType newType, String newText) {
	if (newType == null) {
	    throw(new IllegalArgumentException("need a type!"));
	}
	if (newText == null) {
	    throw(new IllegalArgumentException("need a text!"));
	}
	this.type = newType;
	this.text = newText;
    }
    public String getText() { return text; }
    public TokenType getType() { return type; }
    public String toString() {
	return "("+type+",\""+Utils.quoteString(text)+"\")";
    }

    public boolean isNumeric() {
	if (this.text != null && this.text.matches("^[0-9]+$")) {
	    return true;
	}
	return false;
    }

    public boolean isInternalOperation() {
	if (this.text != null && internalOperationsMap.containsKey(this.text)) {
	    return true;
	}
	return false;
    }

}
