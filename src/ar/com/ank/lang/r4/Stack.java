/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

import java.util.List;
import java.util.ArrayList;

import ar.com.ank.lang.r4.datatypes.Value;

public class Stack {
    private List stack = new ArrayList();
    // TODO - maximum stack size, overload error
    int MAXIMUM_SIZE = 100;

    public void push(Value v) {
	stack.add(v);
    }
    public Value pop() {
	if (stack.isEmpty()) {
	    stackError("Stack Empty but tried to pop a value");
	    return null;
	} else {
	    return (Value) stack.remove(stack.size()-1);
	}
    }
    // 0 = top of stack. 1 = next, etc
    public Value get(int i) {
    	return (Value) stack.get(stack.size()-1-i);
    }

    public String dump() {
	return toString();
    }
    public boolean isEmpty() {
	return stack.isEmpty();
    }
    public String toString() {
	String result = "STACK = ";
	if (stack.isEmpty()) {
	    return "STACK = (empty)\n";
	}
	result += "(top)";
	for (int i = stack.size()-1; i>=0; i--) {
	    Value v = (Value) stack.get(i);
	    if (i<stack.size()-1) {
		result += " ";
	    }
	    result += v.toString();
	}
	result += "(bottom)";
	return result;
    }
    // TODO Errors
    private void stackError(String s) {
	System.err.println("Stack Error: "+s);
	System.exit(0);
    }
}