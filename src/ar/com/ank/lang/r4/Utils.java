/*
  ank_r4. Copyright 2007 Andres N. Kievsky
  
  This file is part of ank_r4.

  ank_r4 is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ank_r4 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ar.com.ank.lang.r4;

public class Utils {
    public static String quoteString(String s) {
	String result = s.replaceAll("\n","\\\\n");
	result = result.replaceAll("\r","\\\\r");
	return result;
    }
    public static int[] stringToIntArray(String s) {
    	char[] p = s.toCharArray();
    	int[] r = new int[p.length];
    	for (int i = 0; i<p.length; i++) {
    		r[i] = p[i];
    	}
    	return r;
    }
    public static String intArrayToString(int[] a) {
    	StringBuffer s = new StringBuffer("");
    	for (int i = 0; i < a.length; i++) {
    		s.append((char)a[i]);
    	}
    	return s.toString();
    }
}
